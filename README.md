# Yl954 Mini Project 6

This project deploys the lambda function with add operation. It also adds logging to the Rust Lambda function, integrate AWS X-Ray tracing and connect logs/traces to CloudWatch.

## Add logging to a Rust Lambda function
In ```Cargo.toml```
```
log = "0.4"
env_logger = "0.9"
```
In ```main.rs```
```
use log::{info, error};
use env_logger;
...
info!("Sum = {:?}", sum);
error!("Invalid request data!");
```

## Integrate AWS X-Ray tracing
Enable the X-Ray active tracing under the ```Configuration > Monitoring and operations tools``` of the deployed lambda function.
![](1.png)

## Connect logs/traces to CloudWatch
1. Create a test event with http Event JSON (with invalid request data)
![](2.png)
2. Go to CloudWatch and check the logs/traces information
![](3.png)
![](4.png)