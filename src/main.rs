use lambda_http::{run, service_fn, Body, Error, Request, Response, RequestExt};
use serde::Serialize;
use log::{info, error};
use env_logger;


#[derive(Serialize)]
struct AddResponse {
    result: i32,
}

async fn function_handler(event: Request) -> Result<Response<Body>, Error> {
    let x1 = event
    .query_string_parameters_ref()
    .and_then(|params| params.first("x1"))
    .unwrap_or("0");

    let x2 = event
    .query_string_parameters_ref()
    .and_then(|params| params.first("x2"))
    .unwrap_or("0");

    let x1_num: i32 = match x1.parse() {
        Ok(data) => data,
        Err(_) => {
            let resp = Response::builder()
                .status(502)
                .body(Body::from("Invalid request data"))
                .unwrap();
            error!("Invalid request data!");
            return Ok(resp);
        }
    };

    let x2_num: i32 = match x2.parse() {
        Ok(data) => data,
        Err(_) => {
            let resp = Response::builder()
                .status(502)
                .body(Body::from("Invalid request data"))
                .unwrap();
            error!("Invalid request data!");
            return Ok(resp);
        }
    };

    let sum = x1_num + x2_num;
    let res_data = AddResponse { result: sum };

    let resp = Response::builder()
    .status(200)
    .header("content-type", "text/html")
    .body(serde_json::to_string(&res_data).unwrap().into())
    .map_err(Box::new)?;
    info!("Sum = {:?}", sum);
    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    env_logger::init();

    run(service_fn(function_handler)).await
}
